CREATE TABLE IF NOT EXISTS Account (
  accountId NUMBER NOT NULL,
  clientId NUMBER NOT NULL,
  accountNumber VARCHAR NOT NULL,
  bankBik VARCHAR NOT NULL,
  amount NUMBER NOT NULL,
  PRIMARY KEY (accountId)
);