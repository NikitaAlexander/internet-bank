package ru.sbt;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.SimpleDriverDataSource;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.function.Consumer;


import static java.sql.DriverManager.getConnection;


/*
1. Нужно сделать базу данных
подключиться к ней
и создать в ней записи
сделать классы для работы с таблицами

2. Подключить к этому всему tomcat
3. сделать ui
4. mvc
*/

public class Main {
    public static void main(String[] args) throws SQLException {

     /*   SimpleDriverDataSource dataSource = new SimpleDriverDataSource();
        dataSource.setDriverClass(h2.Driver.class);


        String data = System.getProperty("user.dir")+"\\src\\resources\\data";
        dataSource.setUrl("jdbc:h2:"+data+"/TEST_DB");

        dataSource.setUsername("admin");
        dataSource.setPassword("secret");

        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
*/



     //   PersonDaoImpl personDao = new PersonDaoImpl(jdbcTemplate);
     //   createTablePerson();
      //  addPerson();
      //  selectPerson();
       // dropTablePersons();
      //  createTablePerson();
    }

    public static void createTablePerson()  throws SQLException
    {
        String data = System.getProperty("user.dir")+"\\src\\resources\\data";
        System.out.println(data);
        try(Connection connection = getConnection("jdbc:h2:"+data+"/TEST_DB", "admin", "secret")){
            PreparedStatement statement = connection.prepareStatement(
                    "CREATE TABLE Persons" +
                            "(" +
                            "id int," +
                            "name varchar(255)," +
                            "login varchar(255)," +
                            "password varchar(255)," +
                            "primary key(id)" +
                            ");");

            statement.execute();
        }

    }


    public static void addPerson()  throws SQLException
    {
        String data = System.getProperty("user.dir")+"\\src\\resources\\data";
        System.out.println(data);
        try(Connection connection = getConnection("jdbc:h2:"+data+"/TEST_DB", "admin", "secret")){
            PreparedStatement statement = connection.prepareStatement(
                    "INSERT INTO Persons(id, name) VALUES(2, 'JOHN2')"
            );

            statement.execute();
        }

    }

    public static void selectPerson()  throws SQLException
    {
        String data = System.getProperty("user.dir")+"\\src\\resources\\data";
        System.out.println(data);
        try(Connection connection = getConnection("jdbc:h2:"+data+"/TEST_DB", "admin", "secret")){
            PreparedStatement statement = connection.prepareStatement(
                    "SELECT id, name FROM PERSONS"
            );

            statement.execute();
            ResultSet resultSet = statement.getResultSet();

           while(resultSet.next()){
               System.out.println(resultSet.getInt(1) + " - " + resultSet.getString(2));
           }
            //connection.close();
        }

    }

    public static void dropTablePersons()  throws SQLException
    {
        String data = System.getProperty("user.dir")+"\\src\\resources\\data";
        System.out.println(data);
        try(Connection connection = getConnection("jdbc:h2:"+data+"/TEST_DB", "admin", "secret")){
            PreparedStatement statement = connection.prepareStatement(
                    "DROP TABLE Persons"
            );

            statement.execute();
        }

    }



}
