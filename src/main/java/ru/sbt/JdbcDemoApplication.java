package ru.sbt;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import javax.annotation.PostConstruct;

@SpringBootApplication
public class JdbcDemoApplication {
//    @Autowired
//    private ShopService shopService;
//
//    @PostConstruct
//    public void init() {
//        shopService.doShopping();
//        shopService.find();
//    }

    public static void main(String[] args) {
        SpringApplication.run(JdbcDemoApplication.class, args);
    }
}
