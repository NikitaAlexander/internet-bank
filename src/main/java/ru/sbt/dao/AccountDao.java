package ru.sbt.dao;

import java.util.List;

/**
 * Created by Никита on 07.11.2016.
 */
public interface AccountDao {
    void save(Account account);

    List<Account> find(int clientId);

}
