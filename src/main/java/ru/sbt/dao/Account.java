package ru.sbt.dao;

/**
 * Created by Никита on 07.11.2016.
 */
public class Account {
    private final int accountId;
    private final int clientId;
    private final String accountNumber;
    private final String bankBik;
    private final double amount;

    public Account(int accountId, int clientId, String accountNumber, String bankBik, double amount) {
        this.accountId = accountId;
        this.clientId = clientId;
        this.accountNumber = accountNumber;
        this.bankBik = bankBik;
        this.amount = amount;
    }

    public int getAccountId() {
        return accountId;
    }

    public int getClientId() {
        return clientId;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public String getBankBik() {
        return bankBik;
    }

    public double getAmount() {
        return amount;
    }
}
