package ru.sbt.dao;

import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static java.util.Collections.singletonMap;

/**
 * Created by Никита on 07.11.2016.
 */
public class AccountDaoImpl implements AccountDao {

    private final NamedParameterJdbcTemplate jdbcTemplate;

    public AccountDaoImpl(NamedParameterJdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    public void save(Account account) {
        Map<String, Object> params = new HashMap<>();
        params.put("accountId", account.getAccountId());
        params.put("clientId", account.getClientId());
        params.put("accountNumber", account.getAccountNumber());
        params.put("bankBik", account.getBankBik());
        params.put("amount", account.getAmount());
        jdbcTemplate.update("INSERT INTO Account(accountId, clientId, accountNumber, bankBik, amount) VALUES(:accountId, :clientId, :accountNumber, :bankBik, :amount)", params);
    }

    @Override
    public List<Account> find(int clientId) {
        return jdbcTemplate.query("SELECT * FROM Account WHERE clientId = :clientId", singletonMap("clientId", clientId),
                (rs, i) -> new Account(rs.getInt("accountId"), rs.getInt("clientId"), rs.getString("accountNumber"), rs.getString("bankBik"), rs.getDouble("amount")));
    }
}
