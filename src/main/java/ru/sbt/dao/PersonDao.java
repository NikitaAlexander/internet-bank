package ru.sbt.dao;
/**
 * Created by Sasha on 07.11.2016.
 */
public interface PersonDao {

    public void add(Person person);

    public void findByLogin(String login);

}
