package ru.sbt.dao;
/**
 * Created by Sasha on 07.11.2016.
 */
public interface BillDao {

    public void add(Bill bill);

    public void update(Bill bill);

    public void delete(int billId);

    public void get(int billId);
}
