package ru.sbt.dao;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Sasha on 07.11.2016.
 */
public class PersonDaoImpl implements PersonDao {
    private final JdbcTemplate jdbcTemplate;

    public PersonDaoImpl(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    public void add(Person person) {
        Map<String, Object> params = new HashMap<>();
        params.put("id",   person.getId());
        params.put("name", person.getName());
        params.put("login", person.getLogin());
        params.put("password", person.getPassword());

        jdbcTemplate.update("INSERT INTO PERSONS(id, name, login, password) VALUES(:id, :name, :login, :password)", params);
    }

    @Override
    public void findByLogin(String login) {

    }
}
