package ru.sbt.dao;
/**
 * Created by Sasha on 08.11.2016.
 */
public class Bill {

    private final int billId;
    private final int idPerson;
    private final int summa;
    private final int billNumber;

    public Bill(int billId, int idPerson, int summa, int billNumber) {
        this.billId = billId;
        this.idPerson = idPerson;
        this.summa = summa;
        this.billNumber = billNumber;
    }

    public int getbillId() {
        return billId;
    }

    public int getIdPerson() {
        return idPerson;
    }

    public int getSumma() {
        return summa;
    }

    public int getBillNumber() {
        return billNumber;
    }

    @Override
    public String toString() {
        return "Bill{" +
                "billId=" + billId +
                ", idPerson=" + idPerson +
                ", summa=" + summa +
                ", billNumber=" + billNumber +
                '}';
    }
}
